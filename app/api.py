""" Main api module """
import os
from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder


app = FastAPI(
    title="anz-test",
    description="ANZ Task",
    docs_url="/doc",
    redoc_url=None
)

@app.get("/version")
async def index():
    """ Index """
    git_sha = os.getenv("GIT_SHA") or "unknown"
    git_tag = os.getenv("GIT_TAG") or "unknown"
    response = {
        "myapplication": [
            {
                "description": "Pre-interview tech task",
                "lastcommitsha": git_sha,
                "version": git_tag
            }
        ]
    }
    return jsonable_encoder(response)
