""" API related tests """
from expects import expect, be_a, have_key, have_length, equal
from starlette.testclient import TestClient
from app.api import app


def test_api():
    """ test /version endpoint """
    client = TestClient(app)
    response = client.get("/version")
    expect(response.status_code).to(equal(200))
    response_body = response.json()
    expect(response_body).to(be_a(dict))
    expect(response_body).to(have_key("myapplication"))
    expect(response_body["myapplication"]).to(be_a(list))
    expect(response_body["myapplication"]).to(have_length(1))
    expect(response_body["myapplication"][0]).to(be_a(dict))
    expect(response_body["myapplication"][0]).to(have_key("description"))
    expect(response_body["myapplication"][0]).to(have_key("version"))
    expect(response_body["myapplication"][0]).to(have_key("lastcommitsha"))
