FROM tiangolo/uvicorn-gunicorn:python3.8
ARG GIT_SHA=""
ARG GIT_TAG=""
ENV GIT_SHA=${GIT_SHA}
ENV GIT_TAG=${GIT_TAG}
ENV PORT 8080
ENV APP_MODULE app.api:app
ENV LOG_LEVEL info
ENV WEB_CONCURRENCY 2

COPY ./requirements/base.txt ./requirements/base.txt
RUN pip install -r requirements/base.txt

COPY ./app /app/app
