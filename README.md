
ANZ Pre-interview Test
---

## Application Overview
This application exposes single `/version` endpoint and returns dynamically generated JSON response.
Generated JSON response contains "version" and "lastcommitsha" fields, that are retrieved from `GIT_TAG` and `GIT_SHA`
environment variables respectively. The values for this environment variables will be set during docker build on CI server.
In case environment variables were not set, `unknown` will be returned instead.

## CI pipeline 
CI pipeline is for GitLab and performs the following steps for every repository push or PR merge:
1. Runs pytest
1. Generates coverage report in Cobertura XML format
1. Generates test results in Junit XML format
1. Runs code quality analysis (pylint)

Docker image building and security scanning will be run only for new tags. Git tag is used to tag docker images.

Docker image security scanning using Anchore was done intentionally after image is pushed to Docker Hub, in order to ensure that
security scanning will not break the pipeline. Finding an image without security vulnerabilities was not in scope of this task. 

##How to run
### Run Locally from source
Prerequisites:
* Python 3
* pip

To run locally from follow the following steps:
1. Install requirements `pip install -r requirements/base.txt`
2. Run application `python main.py`

Open your browser to http://localhost:8080/version

### Run from docker image
To run from docker image run the following command:
`docker run -it --rm -p 8080:8080 kokachev/anz-test:1.0.3`

## Testing
To run unittests with code coverage issue the following command:
```
pip install -r requirements/test.txt
pytest --cov-report=term app --cov=app
```
To run Code Quality (pylint) report, use the following command:
```
pylint app
```

## Deploy to Kubernetes cluster
To deploy to kubernetes cluster run the following command:
```
kubectl create ns technical-test
kubectl -n technical-test apply -f manifests/
```

# FAQ
## Q: What the risks of current implementation/deployment?
A: Current implementation poses the following risks:
* Application's docker image is based on unverified/unknown/untrusted image
* There is no authentication
* Application is exposed via HTTP instead of HTTPS
* Application is running as a root user

## Q:What is your versioning approach?
A: For this application releasing is done by push new Git tag. Alternative options can be used (gitflow, etc).  
